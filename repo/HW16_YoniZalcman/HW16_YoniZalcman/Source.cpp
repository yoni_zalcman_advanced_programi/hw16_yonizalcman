#include "sqlite3.h"
#include <stdio.h>
#include<iostream>

using namespace std;

void main()
{
	int rc,rc2;
	sqlite3* db;
	rc = sqlite3_open("FirstPart.db", &db);
	if (rc)
	{
		cout << "can't open database" << endl;
		sqlite3_close(db);
	}
	else
	{
		sqlite3_exec(db, "create table people id integer primary key autoincrement,name string", NULL, NULL, NULL);
		sqlite3_exec(db, "insert into people(name)values('bob')", NULL, NULL, NULL);
		sqlite3_exec(db, "insert into people(name)values('jim')", NULL, NULL, NULL);
		sqlite3_exec(db, "insert into people(name)values('boy')", NULL, NULL, NULL);
		sqlite3_exec(db, "update people set name='bobi' where id=3", NULL, NULL, NULL);
		sqlite3_close(db);
	}
}